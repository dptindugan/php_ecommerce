<?php

session_start();

unset($_SESSION['cart']);
// Redirects back to the page that requested the empty card page
header("Location: ". $_SERVER["HTTP_REFERER"]);