<?php 

	require "./connection.php";
	// var_dump($_POST);
	// var_dump($_FILES);

	// check how superglobal $_FILES look
	// superglobal $_FILES is an assoc array that will contain a key equivalent to the name given in our file input in the form which has an assoc array of information of the uploaded file as its value

	// check how $_FILES['image'] look
	// ['image'] is the value of our name attribute for our input file
	// $_FILES['image'] will return all the details of the uploaded file in our web server
	// syntax $_FILES['name in the form'][property/key]
	
	

	$name = htmlspecialchars($_POST['productName']);
	$price = htmlspecialchars($_POST['price']);
	$description = htmlspecialchars($_POST['description']);
	$category = 1;

	// get the file extension of $filename using the pathinfo() and convert it to lowercase chars.
	// pathinfo() will return an assoc array of information regarding the path and file type of the uploaded file
	// we are using the PATHINFO_EXTENSION to only return the file extension
	// syntax: pathinfo(file to be checked, option)

	$filename = $_FILES['image']['name'];
	$filesize = $_FILES['image']['size'];
	$file_tmpname = $_FILES['image']['tmp_name'];
	$file_type = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
	// var_dump($file_type);

	$isImg = false;
	$hasDetails = false;
	if($file_type == "jpg" || $file_type== "png" || $file_type == "jpeg" || $file_type == "gif" || $file_type == "svg"){
		$isImg = true;
	}

	if($name != "" && $price >0 && $description != ""){
		$hasDetails = true;
	}


	if($filesize > 0 && $isImg && $hasDetails){
		// lets declare the final path that we want to assign to the uploaded file
		// move the image that is temporarily stored in our server to the final path
		// syntax: move_uploaded_file(temporary_path, new_path);
		$final_filepath = "../assets/images/" . $filename;
		move_uploaded_file($file_tmpname, $final_filepath);

		$new_item_query = "INSERT INTO items(name, price, description, image, category_id) VALUES(\"$name\", $price, \"$description\", \"$final_filepath\", $category)";
		$result = mysqli_query($conn, $new_item_query);
		echo "Product successfully added";



	} else{
		echo "Missing input";
	}

	header("Location: ../views/gallery.php");

	// var_dump($filename);
	// var_dump($filesize);
	// var_dump($file_tmpname);

 ?>