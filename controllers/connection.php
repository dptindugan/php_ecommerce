<?php 
$host = 'localhost'; // determines the host to use either local address of IP
$username = 'root'; //user for the host
$password = '';//password for the host 
$db = "b43_ecom_db";

// open a connection to the MySQL server
// mysqli_connection(host, user, password, database);
$conn = mysqli_connect($host, $username, $password, $db);

if(!$conn){
	// mysqli_error() returns a string description of the last error
	// die("message") prints a message and exits the php script
	die("connection failed:  " . mysqli_error($conn));
}

else{
	// var_dump("successfully connected...");
}
?>