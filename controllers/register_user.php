<?php 
	require "./connection.php";

	// PHP has predefined variables which are designed to collect data sent by HTML form which $_POST and $_GET superglobal variables.
	// superglobal variables simply means that it is a specially pre-defined variable(normally arrays) that can be accessed in the program.

	// the request returned an array where it establish the name attribute of the input as the key and the value inputted to it as its value.

	// var_dump($_POST);
	// var_dump($_POST['fname']);
	// var_dump($_GET);

	// they($_POST and $_GET) do the same thing as both variables handle html form data but the main differenece is when you use the GET method, the query string we entered in the form will be displayed in theURL. POST, on the other hand, sends forms behind the scenes. thus, not seeing the form data in the URL.

	// sanitize our inputs
	$fname = htmlspecialchars($_POST['fname']);
	$lname = htmlspecialchars($_POST['lname']);
	$username = htmlspecialchars($_POST['username']);
	$address = htmlspecialchars($_POST['address']);
	$email = htmlspecialchars($_POST['email']);
	$password = htmlspecialchars($_POST['password']);
	$confirmPassword = htmlspecialchars($_POST['confirm']);
	$role_id = 2;





	if($password != "" || $confirmPassword != ""){
		// lets hash our password to make it secure
		$password = sha1($password);
		$confirmPassword = sha1($confirmPassword);
		// var_dump($password);
		// var_dump($confirmPassword);
		// check if $password is equal to confirm password
		if ($password == $confirmPassword) {
			echo "passwords match <br>";
			
			
			$insert_query = "INSERT INTO users(username, password, firstname, lastname, email, address, role_id) VALUES(\"$username\", \"$password\", \"$fname\", \"$lname\", \"$email\", \"$address\", $role_id)";

			$result = mysqli_query($conn, $insert_query);

			if($result){
				echo "Registered Successfully...";
			}
			else{
				echo mysqli_error($conn);
			}
		
			header('Location: ../views/login.php');



		}else{
			echo "passwords not match";
		}

		// we are going to be storing to a JSON file. JSON stands for
		// Javascript Object Notation. It is used in exchanging
		// and storing data from the web server. JSON uses the object
		// notation/syntax of Javascript
	} else {
		echo "Please check the password fields";
	}

	if($fname != "" && $lname != ""){
		echo "<br>Welcome ".$fname." ".$lname;
	} else {
		echo "please provide a complete name";
	}

	// if $email input is not equal to an empty string, echo out your email is: (value of the $email variable)
	if($email != ""){
		echo "<br>Your email is: ".$email;
	}else{
		echo "<br>please provide an email";
	}
 	
 ?>