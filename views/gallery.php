<?php 
// in order for us to access the stored data in the $_SESSION across different pages, we just initialize the session wiht the session start function syntax: session start()

// once we have initialized the session. we can then freely access all the session variables and its values stored in the current $_SESSION and use it as we please

// if($_SESSION['email'] == "admin@email.com")
	require_once '../partials/header.php';
	function getTitle(){
		return 'Gallery Page';
	}
?>
	<div class="container galleryBG">
		<h2 class="text-center text-light">Products Dashboard</h2>


		<div class="row"> 
			<div class="col-md-12">
				<h2>Sort By:</h2>
				<ul class="list-group border my-5 text-center">
					<li class="list-group-item">
						<a href="../controllers/sort.php?sort=asc">
							Price(Lowest to Highest)
						</a>
					</li>
					<li class="list-group-item">
						<a href="../controllers/sort.php?sort=desc">
							Price(Highest to Lowest)
						</a>
					</li>
				</ul>
			</div>
		<?php 


			if (isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 1) {
				echo "
			
				<div class=\"col-md-4 text-center\">
					<a class='btn btn-primary btnCenter btn-block' href=\"add_product.php\">
						<div class=\"card products h-100 m-2\">
							<div class=\"card-body bodybody\">
								ADD PRODUCT
							</div>
						</div>
					</a>
				</div>


				";
			}

			$product_query = "SELECT * FROM items";
			if(isset($_SESSION['sort'])){
				//product_query concatinate _SESSION['sort']
				$product_query .= $_SESSION['sort'];
			}
/*		 ?>

			<?php */
				$product_array = mysqli_query($conn, $product_query);
				// var_dump($product_query);
				// var_dump($products_array);

				foreach($product_array as $product){
				// var_dump($product);

			 ?>
			<div class="col-md-4">

				<div class="card products  h-100 m-2">
					<div class="card-img-top text-center">
						<img src="<?= $product['image']; ?>" class="p-2" id="imgimg">
					</div>
					<div class="card-body bodybody">
						<h5 class="card-title" ><?= $product['name'] ?></h5>
						<p class="card-text">Price: <?= $product['price'] ?></p>
						<p class="card-blockquote">Description: <?= $product['description']?></p>
					</div><!-- end card -->
					<?php if (isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 2): ?>
						<div class="card-footer">
							<form action="../controllers/update_cart.php" method="POST">
								<input type="number" class="form-control" min="1" value="1" name="item_quantity">
								<input type="hidden" name="item_id" value=<?= $product['id'] ?>>
								<button class="btn-primary btn btn-block add-to-cart">Add To Cart</button>
							</form>
						</div>
					<?php endif ?>

					<?php if (isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 1): ?>
						<div class="card-footer">
							<a href="../controllers/delete_item.php?id=<?=$product['id'];?>" class="btn btn-danger btn-block mt-2">Delete Item</a>
							<a href="./edit_form.php?id=<?=$product['id'];?>" class="btn btn-primary btn-block">Update Item</a>
						</div>
					<?php endif ?>

				</div><!-- end card -->
			</div><!-- end cols -->
			<?php 
				}
			 ?>
		</div><!-- end row -->
	</div><!-- end container -->

<?php 
	require_once '../partials/footer.php';
?>