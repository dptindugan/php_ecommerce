<?php 
	require_once '../partials/header.php';
	function getTitle(){
		return 'Items in cart';
	}
	function addItem(){

	}
?>

<div class="container">
	<h2 class="text-center">My Cart</h2>
	<div class="row">
		<div class="col-md-10 mx-auto">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="cart-items">
					<thead>
						<tr>
							<th>Item</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th><a href="./gallery.php" class="btn btn-block btn-primary">Back to gallery</a></th>
						</tr>
					</thead>
					<tbody>

						<?php 
						if (isset($_SESSION['cart']) && count($_SESSION['cart'] )!= 0) {
						$total = 0;
							# code...
						foreach ($_SESSION['cart'] as $item_id => $item_quantity): 
							$item_query = "SELECT * FROM items WHERE id = $item_id";
							$result = mysqli_query($conn, $item_query);
							$indiv_item = mysqli_fetch_assoc($result);
							// var_dump($indiv_item['price']);

							//convert the assoc array into set of variables w/ associative array keys as the variable names
							extract($indiv_item);
							$subtotal = $price * $item_quantity;
							$total += $subtotal;

							// echo $price;

						?>
						<tr>
							<td><?= $name; ?></td>
							<td><?= $price; ?></td>

							<!-- quantity -->
							<td>
								<form action="../controllers/update_cart.php" method="POST">
									<input type="number" name="item_quantity" class="quantityInput" value=<?= $item_quantity?>>
									<input type="hidden" name="item_id" value=<?= $id?>>
									<input type="hidden" name="updateFromCart" value="true">
									<button type="submit" class="btn btn-primary">Update Quantity</button>
								</form>
							</td>

							<!-- subtotal -->
							<td><?= number_format($subtotal, 2); ?></td>

							<!-- Remove Cart -->
							<td>
								<form action="../controllers/remove_from_cart.php" method="POST">
									<input type="hidden" name="item_id" value=<?=$item_id?>>
									<button class="btn-danger btn btn-block">Remove From Cart</button>
								</form>
							</td>
						</tr>
						<?php endforeach ?>
					
						
						<tr>
							<td></td>
							<td></td>
							<td>Total: <?= number_format($total, 2); ?></td>
							<td><a href="../controllers/checkout.php" class="btn btn-primary btn-block">Checkout</a></td>
							<td><a href="../controllers/empty_cart.php" class="btn btn-outline-danger btn-block">Empty Cart</a></td>
						</tr>

					<?php }

						else{
							echo 
							"<tr>
								<td class='text-center' colspan='6'>
									NO ITEMS IN THE CART
									
								</td>
							</tr>
							";
						}

					?>


					</tbody>
				</table>
		</div>
	</div>					
</div>


<?php 
	require_once '../partials/footer.php';
 ?>