<?php 
	
	require '../partials/header.php';
	function getTitle(){
		return "Login Page";
	}
 ?>
	

		
	<section class="login-form">
		<h1>Log-in</h1>

		<form class="form-group" action="../controllers/authenticate.php" method="POST">
			
		<!-- 	<label for="welcome-label">Welcome: </label>
			<select id="welcome-label" class="form-control text-justify">
				<option selected="" id="admin">Admin</option>
				<option id="user">User</option>
			</select> -->


			<div class="input-box mt-5">
				<input type="email" name="email" id="email" required="@" class="form-control">
				<label for ="email">Email:</label>
			</div>

			
			<div class="input-box mt-5">
				<input type="password" name="password" id="password" required="" class="form-control">
				<label for="password">Password:</label>
			</div>
<!-- 			<input type="submit" name="submit" value="Log in" class="btn btn-block btn-outline-info mt-3 login-button-mobile"> -->
			<button type="submit" name="log-in" class="btn btn-block btn-primary mt-3 login-media">Log In</button>
			<a href="./register.php">Register an account</a>
			<a href="">Forgot Password?</a>
		</form>
	</section>


<?php 
	require "../partials/footer.php";

?>