<?php 
	// the require statement is the same as teh include statement in the sense that it allows devs to reference files from a different location. the only difference is the error handling will throw a warning but still execute the code while require will throw a fatal error. include_once and require_once does the same thing but once the file is included/required already, it will not execute again
	// include "../partials/header.php";
	// require_once '../partials/header.php';
	require '../partials/header.php';
	function getTitle(){
		return "Register Page";
	}
 ?>



	<section class="register-form">
		<h1>Registration Page</h1>
		<!-- the action attribute sets the destination to which the form data is submitted. the value of this can be an absolute or relative url -->
		<!-- the method attribute specifies the type of HTTP request you want to make when sending the form data -->

		<form class="form-group" action="../controllers/register_user.php" method="POST">

			<div class="input-box mt-4">
				<input type="text" name="fname" id="fname" required="" class="form-control">
				<label for ="fname">Firstname:</label>
			</div>
			<div class="input-box mt-4">
				<input type="text" name="lname" id="lname" required="" class="form-control">
				<label for ="lname">Lastname:</label>
			</div>
			<div class="input-box mt-4">
				<input type="text" name="address" id="address" required="" class="form-control">
				<label for ="address">Address:</label>
			</div>
			<div class="input-box mt-4">
				<input type="text" name="username" id="username" required="" class="form-control">
				<label for ="username">Username:</label>
			</div>
			<div class="input-box mt-4">
				<input type="email" name="email" id="email" required="" class="form-control">
				<label for ="email">Email:</label>
			</div>
			<div class="input-box mt-4">
				<input type="password" name="password" id="password" required="" class="form-control">
				<label for ="password">Password:</label>
			</div>
			
			<div class="input-box mt-4">
				<input type="password" name="confirm" id="confirm" required="" class="form-control">
				<label for="confirm">Confirm Password:</label>
			</div>
	<!-- 		<input type="submit" name="submit" value="Log in" class="btn btn-block btn-outline-info mt-3 login-button-mobile"> -->
			<button type="submit" name="submit" class="btn btn-block btn-primary mt-3">Register</button>
		</form>
	</section>

	<?php 
		require "../partials/footer.php";
	 ?>