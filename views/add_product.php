<?php 
	require_once "../partials/header.php";
	function getTitle(){
		return "Add Item Page";
	};
 ?>

 	<div class="container-fluid">
 		<h2 class="text-center">Add New Item</h2>
 		<div class="row">
 			
 			<div class="col-md-8 mx-auto">
 				<form action="../controllers/add_item.php" method="POST" enctype="multipart/form-data">
 					<div class="form-group">
	 					<label for="productName" class="text-light">Product Name</label>
	 					<input id="productName" type="text" class="form-control" name="productName">
 					</div>

 					<div class="form-group">
	 					<label for="price" class="text-light">Price</label>
	 					<input id="price" type="number" class="form-control" name="price">
 					</div>

 					<div class="form-group">
	 					<label for="description" class="text-light">Decription</label>
	 					<input id="description" type="text" class="form-control" name="description">
 					</div>

 					<div class="form-group">
	 					<label for="image" class="text-light">Image</label>
	 					<input id="image" type="file" class="form-control" name="image">
 					</div>

 					<button type="submit" class="btn btn-primary">Add New Item</button>
<?php  		/*			<?php 
 					if (isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 1) {
 						
	 					foreach ($_SESSION[''] as $key => $value): ?>
		 					<select>
		 						<option></option>
		 					</select>
 					<?php endforeach }?>*/

 					 ?>



 				</form>
 			</div>
 		</div>
 	</div>

 <?php 
 	require_once '../partials/footer.php'
  ?>