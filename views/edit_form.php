<?php

require_once '../partials/header.php';

function getTitle(){
	return 'Update details';
}

	$item_to_be_edited = $_GET['id'];
	// var_dump($item_to_be_edited);

	// query that retrieves the item from the items table where the value of the id column matches the value of $item_to_be_edited
	$item_query = "SELECT name, price, description FROM items WHERE id = $item_to_be_edited";
	$item_result = mysqli_query($conn, $item_query);
	$item = mysqli_fetch_assoc($item_result);
	// var_dump($item);
	extract($item);
	// var_dump($name);
?>

<div class="container">
	<h2 class="text-center">Update Items</h2>
	<div class="row">
		<div class="col-md-10 mx-auto">
			<form action="../controllers/update_item.php" method="POST">
				<div>
					<label for="itemName">Item name:</label>
					<input type="text" name="item_name" class="form-control" id="itemName" value="<?= $name ?>" placeholder="Enter Item Name Here">
				</div>

				<div>
					<label for="itemPrice">Item price:</label>
					<input type="text" name="item_price" class="form-control" id="itemPrice" value="<?= $price?>" placeholder="Enter Price here">
				</div>

				<div>
					<label for="description">Description</label>
					<textarea class="form-control" name="description" id="description" rows="10" placeholder="Enter your description here"><?=$description?></textarea>
				</div>

				<input type="hidden" name="id" value=<?=$item_to_be_edited?>>

				<button class="btn btn-primary" type="submit">Update</button>
			</form>
		</div>
	</div>
</div>


<?php 
require_once '../partials/footer.php';


?>