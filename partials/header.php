<?php 
	session_start();
	// var_dump($_SESSION);
	require_once "../controllers/connection.php"
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

	<title>php card store| <?php echo getTitle() ?></title>

	<!-- icon -->
	


	<!-- animate css -->
	<!-- <link rel="stylesheet" type="text/css" href="./assets/css/animate.css"> -->

 	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Overpass+Mono&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Space+Mono&display=swap" rel="stylesheet">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- external css -->
	<link rel="stylesheet" href="../assets/css/registration.css">
	<link rel="stylesheet" href="../assets/css/style.css">

	<!-- fontawesome icons -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/d5912da5cf.js" crossorigin="anonymous"></script>

</head>
<body>

	<nav class="navbar navbar-expand-lg bg-dark sticky-top nav-pills">

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbar-nav">
			<ul class="navbar-nav mr-auto" >
				<?php 
					// isset() function checks if a variable/element has been assigned a value
					// if session email has no assigned value/has not been set. display register the logout button
					// var_dump($_SESSION["email"]);
			
					// var_dump($_SESSION['user']['firstname']); //returns the value of the firstname field of the current owner of $_SESSION['user']
					// var_dump($_SESSION['user']['email']); //returns the value of the firstname field of the current owner of $_SESSION['user']

					if(!isset($_SESSION['user']['firstname'])){
						echo"
							<li class='nav-item px-3'>
								<a class='nav-link' href='./gallery.php'>Welome: Guest</a>
							</li>
							<li class='nav-item px-3'>
								<a class='nav-link' href='./register.php'>Register</a>
							</li>
							<li class='nav-item px-3'>
								<a class='nav-link' href='./login.php'>Login</a>
							</li>
							<li class='nav-item px-3'>
								<a class='nav-link' href='./gallery.php'>Gallery</a>
							</li>

						";
						
					}else{
						$username = $_SESSION['user']['firstname'];
					
					
						// meaning someone is logged in
						echo "
							<li class='nav-item'>
								<a href='./gallery.php' class='nav-link'> Welcome: $username !</a>
							</li>
							<li class='nav-item'>
								<a class='nav-link' href='./logout.php'>logout</a>
							</li>
							<li class='nav-item'>
								<a class='nav-link' href='./cart.php'><i class=\"fas fa-shopping-cart\">View Cart</i></a>
							</li>
						";
					}
			
				 ?>
				 <!-- <i class="fas fa-shopping-cart"></i> -->

<!-- 
				<li class="nav-item">
					<a href="./home.php" class="nav-link">Home</a>
				</li>
				<li class="nav-item">
					<a href="./register.php" class="nav-link">Register</a>
				</li>
				<li class="nav-item">
					<a href="./login.php" class="nav-link">Login</a>
				</li> -->
				
			</ul>

		</div>

	</nav>