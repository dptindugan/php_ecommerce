am session
- folder structure
- registration page
- login page

pm session
- add item
- display items

This is a sassy version of the demon doll straight from the scary movie you loved as a kid.

CREATE TABLE items(
	id INT AUTO_INCREMENT NOT NULL,
	name VARCHAR(255),
	price DECIMAL(10,2),
	description VARCHAR(255),
	image VARCHAR(255),
	category_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(category_id) REFERENCES categories(id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);

CREATE TABLE users( id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL,
email VARCHAR(255) NOT NULL UNIQUE,
address VARCHAR(255) NOT NULL,
role_id INT,
PRIMARY KEY(id),
FOREIGN KEY(role_id)
REFERENCES roles(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);


CREATE TABLE statuses(
id INT AUTO_INCREMENT NOT NULL,
name VARCHAR(255),
PRIMARY KEY(id)
);

CREATE TABLE payment_modes(
id INT AUTO_INCREMENT NOT NULL,
name VARCHAR(255),
PRIMARY KEY(id)
);

CREATE TABLE orders (
	id INT AUTO_INCREMENT NOT NULL,
	user_id INT,
	transaction_code VARCHAR(255),
	purchase_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	total DECIMAL(10,2),
	status_id INT,
	payment_mode_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(user_id)
	REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(status_id)
	REFERENCES statuses(id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
	FOREIGN KEY(payment_mode_id)
	REFERENCES payment_modes(id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);

CREATE TABLE item_order(
id INT AUTO_INCREMENT NOT NULL,
order_id INT,
item_id INT,
quantity INT NOT NULL,
PRIMARY KEY(id),
FOREIGN KEY(order_id)
REFERENCES orders(id)
ON UPDATE CASCADE
ON DELETE RESTRICT,
FOREIGN KEY(item_id)
REFERENCES items(id)
ON DELETE RESTRICT
ON UPDATE CASCADE
);

INSERT INTO categories(name) VALUES("Magic Card");
INSERT INTO categories(name) VALUES("Trap Card");
INSERT INTO categories(name) VALUES("Monster Card");

INSERT INTO items (name, price, description, image, category_id) VALUES ("Secret Village of The Spellcasters", 239.70, "Rare magic card", "null", 1);

INSERT INTO items (name, price, description, image, category_id) VALUES ("Metaverse", 175.44, "Rare trap card", "null", 2);

INSERT INTO items (name, price, description, image, category_id) VALUES ("Solemn Judgement", 373.32, "Ultra Rare trap card", "null", 2);

INSERT INTO items (name, price, description, image, category_id) VALUES ("Spellbook Magician of Prophecy", 56.61, "Common monster card", "null", 3);



create 2 roles in the roles table.
	- admin
	- user



create a COD payment mode in the payment_modes table
INSERT INTO payment_modes(name) VALUES("cod");

create the ff statuses:
	- pending
	- completed
	- cancelled

INSERT INTO statuses(name) VALUES("pending");
INSERT INTO statuses(name) VALUES("completed");
INSERT INTO statuses(name) VALUES("cancelled");

// secret vill
--> If only you control a Spellcaster monster, your opponent cannot activate Spell Cards. If you control no Spellcaster monsters, you cannot activate Spell Cards.

// metaverse
// Take 1 Field Spell from your Deck, and either activate it or add it to your hand.

// spellbook magician of prophecy
If this card is Normal Summoned or flipped face-up: Add 1 "Spellbook" Spell from your Deck to your hand.

// Solemn Judgement
// When a monster(s) would be Summoned, OR a Spell/Trap Card is activated: Pay half your LP; negate the Summon or activation, and if you do, destroy that card.




